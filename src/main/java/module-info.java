module mesh.renderer {
	exports de.grogra.mesh.renderer;
	exports de.grogra.mesh.renderer.handler;
	exports de.grogra.mesh.utils;

	requires vecmath;
	requires imp;
	requires imp3d;
	
	requires graph;
	requires xl.core;
	requires platform;
	requires platform.core;
	requires math;
	requires java.desktop;
	requires utilities;
}
