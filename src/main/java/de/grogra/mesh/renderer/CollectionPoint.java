package de.grogra.mesh.renderer;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import de.grogra.imp.PickList;
import de.grogra.imp.View;
import de.grogra.imp3d.RenderState;
import de.grogra.imp3d.View3D;
import de.grogra.imp3d.objects.Attributes;
import de.grogra.mesh.utils.FloatListArray;
import de.grogra.pf.ui.Workbench;

/**
 * A collection that store information for displaying a graph of points. 
 */
public class CollectionPoint extends CollectionDisplay {

	// size of a point when drawn on screen
	float pointSize = 3.0f;
	// enh:field getter setter
	
	// store point locations (x,y,z) as triple of floats
	float[] points;
	// enh:field getter setter

	public CollectionPoint() {
		super();
		points = new float[0];
	}
	
	@Override
	protected void drawImpl(Object object, boolean asNode, RenderState rs) {
		loadPoints(rs, View3D.getDefaultView (Workbench.current()));
		if (points!=null) {
			rs.drawPointCloud(points, pointSize, color, RenderState.CURRENT_HIGHLIGHT, null);
		}
	}

	@Override
	protected void pickImpl(Object object, boolean asNode, Point3d origin, Vector3d direction, Matrix4d transformation,
			PickList list) {
		// TODO Auto-generated method stub
		
	}
	
	//This load the polygons in the shape array. If the array is empty return an empty mesh
	private void loadPoints(RenderState rs, View v) {
		if (!update) {
			return;
		}
		points=null;
		this.setUpdate(false);
		FloatListArray vertices = getVertices(rs,v);
		points = vertices.toArray(3);
	}
	
	public FloatListArray getVertices(RenderState rs, View v) {
		MergeShapeVisitor visitor = new MergeShapeVisitor(boxSize,lineSize);
		visitor.init(rs.getRenderGraphState(), (View3D) v, false, this);
		this.getGraph().accept(this, visitor, null);
		return visitor.getVertices();
	}
	
	// enh:insert $TYPE.addIdentityAccessor (Attributes.SHAPE);
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;

	public static final NType.Field pointSize$FIELD;
	public static final NType.Field points$FIELD;

	private static final class _Field extends NType.Field
	{
		private final int id;

		_Field (String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			super (CollectionPoint.$TYPE, name, modifiers, type, componentType);
			this.id = id;
		}

		@Override
		public void setFloat (Object o, float value)
		{
			switch (id)
			{
				case 0:
					((CollectionPoint) o).pointSize = (float) value;
					return;
			}
			super.setFloat (o, value);
		}

		@Override
		public float getFloat (Object o)
		{
			switch (id)
			{
				case 0:
					return ((CollectionPoint) o).getPointSize ();
			}
			return super.getFloat (o);
		}

		@Override
		protected void setObjectImpl (Object o, Object value)
		{
			switch (id)
			{
				case 1:
					((CollectionPoint) o).points = (float[]) value;
					return;
			}
			super.setObjectImpl (o, value);
		}

		@Override
		public Object getObject (Object o)
		{
			switch (id)
			{
				case 1:
					return ((CollectionPoint) o).getPoints ();
			}
			return super.getObject (o);
		}
	}

	static
	{
		$TYPE = new NType (new CollectionPoint ());
		$TYPE.addManagedField (pointSize$FIELD = new _Field ("pointSize", 0 | _Field.SCO, de.grogra.reflect.Type.FLOAT, null, 0));
		$TYPE.addManagedField (points$FIELD = new _Field ("points", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (float[].class), null, 1));
		$TYPE.addIdentityAccessor (Attributes.SHAPE);
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new CollectionPoint ();
	}

	public float getPointSize ()
	{
		return pointSize;
	}

	public void setPointSize (float value)
	{
		this.pointSize = (float) value;
	}

	public float[] getPoints ()
	{
		return points;
	}

	public void setPoints (float[] value)
	{
		points$FIELD.setObject (this, value);
	}

//enh:end
	
}
