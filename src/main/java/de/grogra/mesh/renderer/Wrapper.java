package de.grogra.mesh.renderer;

import de.grogra.graph.GraphState;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.View3D;
import de.grogra.imp3d.objects.PolygonMesh;
import de.grogra.pf.registry.Item;
import de.grogra.pf.ui.Context;

public class Wrapper {

	
	public static void render(Item item, Object info, Context ctx)  {
		
		Node root = ctx.getWorkbench().getRegistry().getProjectGraph().getRoot();
		GraphState gs = GraphState.get (ctx.getWorkbench().getRegistry().getProjectGraph(), ctx.getWorkbench ().getJobManager ().getThreadContext ());
		MergeShapeVisitor msv = new MergeShapeVisitor();
		msv.init(gs, View3D.getDefaultView(ctx), false, root);
		ctx.getWorkbench().getRegistry().getProjectGraph().accept(root, msv, null);	
		
		PolygonMesh mesh = new PolygonMesh();
	}
	
}
