package de.grogra.mesh.renderer;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import de.grogra.graph.ContextDependent;
import de.grogra.graph.GraphState;
import de.grogra.imp.PickList;
import de.grogra.imp.View;
import de.grogra.imp3d.PolygonArray;
import de.grogra.imp3d.Polygonizable;
import de.grogra.imp3d.Polygonization;
import de.grogra.imp3d.RenderState;
import de.grogra.imp3d.View3D;
import de.grogra.imp3d.objects.Attributes;
import de.grogra.imp3d.objects.PolygonMesh;
import de.grogra.imp3d.objects.Polygons;
import de.grogra.imp3d.objects.Sphere;
import de.grogra.mesh.utils.FloatListArray;
import de.grogra.mesh.utils.PolygonMeshArray;
import de.grogra.pf.ui.Workbench;
import de.grogra.xl.util.DoubleList;

/**
 * A collection to display a graph of polygonizable object into meshes.
 * The mesh is cached and can be reused. It require a lot of memory
 */
public class CollectionMesh extends CollectionDisplay implements Polygonizable {

	protected int visibleSides = Attributes.VISIBLE_SIDES_BOTH;
	//enh:field attr=Attributes.VISIBLE_SIDES getter setter
	
	protected Polygons polygons;
	//enh:field attr=Attributes.POLYGONS getter setter

	public CollectionMesh() {
		super();
		this.polygons=null;
	}
	
	protected void drawImpl(Object object, boolean asNode, RenderState rs) {
		loadShapes(rs, View3D.getDefaultView (Workbench.current()));
		if (polygons!=null) {
			rs.drawPolygons(this, object, asNode, null, -1, true, null);
		}
	}
	
	protected void pickImpl(Object object, boolean asNode, Point3d origin, Vector3d direction, Matrix4d transformation,
			PickList list) {
		if (!(this.polygons instanceof PolygonMesh))
	    {
	    	Sphere.pick (1, origin, direction, list);
	    	return;
	    }
		
		TriangleRayTest test = new TriangleRayTest( origin , direction , transformation );
		test.setPolygons( (PolygonMesh) this.polygons );

		DoubleList lengths = test.intersectionTest( );
		for (int i = 0; i < lengths.size; i++) {
			list.add(lengths.get(i));
		}
	}
	
	public FloatListArray getVertices(RenderState rs, View v) {
		MergeShapeVisitor visitor = new MergeShapeVisitor(boxSize,lineSize);
		visitor.init(rs.getRenderGraphState(), (View3D) v, false, this);
		this.getGraph().accept(this, visitor, null);
		return visitor.getVertices();
	}
	
	//This load the polygons in the shape array. If the array is empty return an empty mesh
	private void loadShapes(RenderState rs, View v) {
		if (!update) {
			return;
		}
		polygons = null;
		this.setUpdate(false);
		//TODO: change to polygonmesharray
		FloatListArray vertices = getVertices(rs, View3D.getDefaultView (Workbench.current()));
		if (!vertices.currentList.isEmpty()) {
			PolygonMeshArray polygonMesh = new PolygonMeshArray();
			float[] verticesArray = vertices.toArray(3);
			vertices.clear();
			int[] tmp = new int[verticesArray.length/3];
			for(int i = 0; i<tmp.length; i++) tmp[i]=i;
			polygonMesh.setIndexData(tmp);
			polygonMesh.setVertexData(verticesArray);
			this.setPolygons(polygonMesh);
		}
	}
	
	@Override
	public ContextDependent getPolygonizableSource(GraphState gs) {
		return polygons;
	}

	@Override
	public Polygonization getPolygonization() {
		final class Poly implements Polygonization
		{
			final int visibleSides = this.visibleSides;
			
			@Override
			public void polygonize (ContextDependent source, GraphState gs, PolygonArray out, int flags, float flatness)
			{
				polygonizeImpl (source, gs, out, flags, flatness);
			}

			@Override
			public boolean equals (Object o)
			{
				if (!(o instanceof Poly))
				{
					return false;
				}
				Poly p = (Poly) o;
				return (p.visibleSides == visibleSides);
			}

			@Override
			public int hashCode ()
			{
				return visibleSides;
			}
		}

		return new Poly ();
	}

	void polygonizeImpl (ContextDependent source, GraphState gs, PolygonArray out,
						 int flags, float flatness)
	{
		if (polygons == null)
		{
			out.init (3);
		}
		else
		{
			polygons.polygonize (source, gs, out, flags, flatness);
			out.visibleSides = visibleSides;
		}
	}
	
	//weird picking algo from the meshnode
	private class TriangleRayTest {

		private final Point3d origin;
		private final Vector3d direction;
		private PolygonMesh polygons;

		public TriangleRayTest(Point3d origin, Vector3d direction, Matrix4d transformation) {
			this.origin = new Point3d( origin );
			this.direction = new Vector3d( direction );
		}

		public void setPolygons( PolygonMesh polygons ) {

			this.polygons = polygons;
		}

		/**
		 * Implementation after Tomas Möller, Eric Haines: Real-Time Rendering, p.
		 * 305
		 *
		 * @return Returns distance to intersection point of intersected triangle
		 */
		public DoubleList intersectionTest() {

			DoubleList results = new DoubleList();

			// if (true) return 0;
			float epsilon = 0.00001f;
			float[] vertices = this.polygons.getVertexData( );
			int[] indices = this.polygons.getIndexData( );

			// iterating over triangles
			for (int i = 0; i <= indices.length - 3; i += 3) {// iterating over triangles
				// vertices of the triangle
				Point3d v0 = new Point3d( vertices[indices[i + 0] * 3] , vertices[indices[i + 0] * 3 + 1] , vertices[indices[i + 0] * 3 + 2] );
				Point3d v1 = new Point3d( vertices[indices[i + 1] * 3] , vertices[indices[i + 1] * 3 + 1] , vertices[indices[i + 1] * 3 + 2] );
				Point3d v2 = new Point3d( vertices[indices[i + 2] * 3] , vertices[indices[i + 2] * 3 + 1] , vertices[indices[i + 2] * 3 + 2] );

				// edge vectors
				Point3d e1 = new Point3d( v1.x - v0.x , v1.y - v0.y , v1.z - v0.z ); //for u
				Point3d e2 = new Point3d( v2.x - v0.x , v2.y - v0.y , v2.z - v0.z ); //for v

				// calculate angle between direction and triangle plane
				Vector3d p = new Vector3d( direction );
				p.cross( p , new Vector3d( e2.x , e2.y , e2.z ) );

				double a = e1.x * p.x + e1.y * p.y + e1.z * p.z;
				if (Math.abs(a) < epsilon)
					continue; // no intersection, test
				// next triangle
				double f = 1 / a;

				// vector from vertice 0 to origin
				Vector3d s = new Vector3d( origin.x - v0.x , origin.y - v0.y , origin.z - v0.z );
				double u = f * s.dot( p );
				if ((u < 0) || (u > 1))
					continue;
				Vector3d q = new Vector3d( s );
				q.cross( q , new Vector3d( e1.x , e1.y , e1.z ) );
				double v = f * direction.dot( q );
				if (v < 0 || (u + v) > 1)
					continue;

				/*
				 * Now we have (u,v,t) in barycentric coordinates we need a distance as
				 * return value
				 */
				double result = f*(new Vector3d(e2.x, e2.y, e2.z).dot( q ));
				if (result < 0)
					continue;
				results.add(result);
			}
			return results;
		}
	}


	// enh:insert $TYPE.addIdentityAccessor (Attributes.SHAPE);
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;

	public static final NType.Field visibleSides$FIELD;
	public static final NType.Field polygons$FIELD;

	private static final class _Field extends NType.Field
	{
		private final int id;

		_Field (String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			super (CollectionMesh.$TYPE, name, modifiers, type, componentType);
			this.id = id;
		}

		@Override
		public void setInt (Object o, int value)
		{
			switch (id)
			{
				case 0:
					((CollectionMesh) o).visibleSides = (int) value;
					return;
			}
			super.setInt (o, value);
		}

		@Override
		public int getInt (Object o)
		{
			switch (id)
			{
				case 0:
					return ((CollectionMesh) o).getVisibleSides ();
			}
			return super.getInt (o);
		}

		@Override
		protected void setObjectImpl (Object o, Object value)
		{
			switch (id)
			{
				case 1:
					((CollectionMesh) o).polygons = (Polygons) value;
					return;
			}
			super.setObjectImpl (o, value);
		}

		@Override
		public Object getObject (Object o)
		{
			switch (id)
			{
				case 1:
					return ((CollectionMesh) o).getPolygons ();
			}
			return super.getObject (o);
		}
	}

	static
	{
		$TYPE = new NType (new CollectionMesh ());
		$TYPE.addManagedField (visibleSides$FIELD = new _Field ("visibleSides", _Field.PROTECTED  | _Field.SCO, de.grogra.reflect.Type.INT, null, 0));
		$TYPE.addManagedField (polygons$FIELD = new _Field ("polygons", _Field.PROTECTED  | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (Polygons.class), null, 1));
		$TYPE.declareFieldAttribute (visibleSides$FIELD, Attributes.VISIBLE_SIDES);
		$TYPE.declareFieldAttribute (polygons$FIELD, Attributes.POLYGONS);
		$TYPE.addIdentityAccessor (Attributes.SHAPE);
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new CollectionMesh ();
	}

	public int getVisibleSides ()
	{
		return visibleSides;
	}

	public void setVisibleSides (int value)
	{
		this.visibleSides = (int) value;
	}

	public Polygons getPolygons ()
	{
		return polygons;
	}

	public void setPolygons (Polygons value)
	{
		polygons$FIELD.setObject (this, value);
	}

//enh:end
	
}
