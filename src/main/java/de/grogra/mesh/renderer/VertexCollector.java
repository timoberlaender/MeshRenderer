package de.grogra.mesh.renderer;

import javax.vecmath.Matrix3d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Point4d;
import javax.vecmath.Tuple2f;
import javax.vecmath.Tuple3f;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;

import de.grogra.graph.GraphState;
import de.grogra.imp3d.Polygonizable;
import de.grogra.imp3d.RenderState;
import de.grogra.imp3d.objects.PolygonMesh;
import de.grogra.imp3d.shading.Shader;
import de.grogra.math.Pool;
import de.grogra.math.TMatrix4d;
import de.grogra.mesh.utils.FloatListArray;
import de.grogra.vecmath.Math2;

public class VertexCollector implements RenderState {

	FloatListArray vertices = new FloatListArray();

	MergeShapeVisitor visitor;
	private float boxSize;
	private float lineSize;
	private boolean minimal;

	public VertexCollector(MergeShapeVisitor v) {
		boxSize  = v.boxSize;
		lineSize = v.lineSize;
		minimal =v.minimalDisplay;
		this.visitor = v;
	}

	public FloatListArray getVertices() {
		return vertices;
	}

	public void clearVertices() {
		vertices.clear();
	}

	@Override
	public GraphState getRenderGraphState() {
		return visitor.getGraphState();
	}

	public final Pool pool = new Pool();

	@Override
	public Pool getPool() {
		return pool;
	}

	@Override
	public Shader getCurrentShader() {
		return visitor.getCurrentShader();
	}

	@Override
	public java.awt.FontMetrics getFontMetrics(java.awt.Font font) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getCurrentHighlight() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public float estimateScaleAt(Tuple3f point) {
		// TODO Auto-generated method stub
		return 1;
	}

	/*
	 * ---------------------------------------------------------------- get the
	 * vertices from shapes
	 */

	private Matrix4d getTransformation(Matrix4d t) {
		if (t == null) {
			t = visitor.getCurrentTransformation();
		} else {
			Math2.mulAffine(xform, visitor.getCurrentTransformation(), t);
			t = xform;
		}
		return t;
	}

	private Shader getShader(Shader s) {
		if (s == null) {
			s = visitor.getCurrentShader();
		}
		return s;
	}

	// ?
	private final Matrix4d xform = new TMatrix4d();


	@Override
	public void drawPoint(Tuple3f origin, int size, Tuple3f color, int highlight, Matrix4d t) {
		t = getTransformation(t);
//		if(minimal) {
//
//			float p0x= (float) t.m03-boxSize;
//			float p0y= (float) t.m13-boxSize;
//			float p0z= (float) t.m23-boxSize;
//
//			float p1x= (float) t.m03+boxSize;
//			float p1y= (float) t.m13-boxSize;
//			float p1z= (float) t.m23-boxSize;
//
//			float p2x= (float) t.m03;
//			float p2y= (float) t.m13+boxSize;
//			float p2z= (float) t.m23-boxSize;
//
//			float p3x= (float) t.m03;
//			float p3y= (float) t.m13;
//			float p3z= (float) t.m23+boxSize;
//
//			vertices
//			.push(p0x).push(p0y).push(p0z)
//			.push(p1x).push(p1y).push(p1z)
//			.push(p2x).push(p2y).push(p2z)
//
//			.push(p0x).push(p0y).push(p0z)
//			.push(p1x).push(p1y).push(p1z)
//			.push(p3x).push(p3y).push(p3z)
//
//			.push(p1x).push(p1y).push(p1z)
//			.push(p2x).push(p2y).push(p2z)
//			.push(p3x).push(p3y).push(p3z)
//			;
//		}else {
//
//			float x0 = (float) t.m03 - boxSize;
//			float y0 = (float) t.m13 - boxSize;
//			float z0 = (float) t.m23 - boxSize;
//			float x1 = (float) t.m03 + boxSize;
//			float y1 = (float) t.m13 + boxSize;
//			float z1 = (float) t.m23 + boxSize;
//
//			float[] p = new float[] { x0, y0, z1, x0, y0, z0, x1, y0, z0, x0, y0, z1, x1, y0, z0, x1, y0, z1, x1, y0, z1,
//					x1, y0, z0, x1, y1, z0, x1, y0, z1, x1, y1, z0, x1, y1, z1, x1, y1, z1, x1, y1, z0, x0, y1, z0, x1, y1,
//					z1, x0, y1, z0, x0, y1, z1, x0, y1, z1, x0, y1, z0, x0, y0, z0, x0, y1, z1, x0, y0, z0, x0, y0, z1, x1,
//					y1, z0, x1, y0, z0, x0, y0, z0, x0, y0, z0, x0, y1, z0, x1, y1, z0, x0, y0, z1, x1, y0, z1, x1, y1, z1,
//					x1, y1, z1, x0, y1, z1, x0, y0, z1, };
//
//			vertices.addAll(p, 0, p.length);
//		}
		vertices.push((float) t.m03, (float) t.m13, (float) t.m23);
	}

	@Override
	public void drawBox(float halfWidth, float halfLength, float height, Shader s, int highlight, boolean asWireframe,
			Matrix4d m) {
		m = getTransformation(m);
		s = getShader(s);

		float x0 = -halfWidth;
		float y0 = -halfLength;
		float z0 = 0;
		float x1 = halfWidth;
		float y1 = halfLength;
		float z1 = height;

		Point4d[] p = new Point4d[] {new Point4d (x0, y0, z0, 1),
				new Point4d (x0, y0, z1, 1), new Point4d (x1, y0, z0, 1),
				new Point4d (x1, y0, z1, 1), new Point4d (x1, y1, z0, 1),
				new Point4d (x1, y1, z1, 1), new Point4d (x0, y1, z0, 1),
				new Point4d (x0, y1, z1, 1)};

		for (int i = 0; i < p.length; i++) {
			m.transform (p[i]);
		}

		vertices.push((float)p[1].x).push((float)p[1].y).push((float)p[1].z)
		.push((float)p[0].x).push((float)p[0].y).push((float)p[0].z)
		.push((float)p[2].x).push((float)p[2].y).push((float)p[2].z)

		.push((float)p[1].x).push((float)p[1].y).push((float)p[1].z)
		.push((float)p[2].x).push((float)p[2].y).push((float)p[2].z)
		.push((float)p[3].x).push((float)p[3].y).push((float)p[3].z)

		.push((float)p[3].x).push((float)p[3].y).push((float)p[3].z)
		.push((float)p[2].x).push((float)p[2].y).push((float)p[2].z)
		.push((float)p[4].x).push((float)p[4].y).push((float)p[4].z)

		.push((float)p[3].x).push((float)p[3].y).push((float)p[3].z)
		.push((float)p[4].x).push((float)p[4].y).push((float)p[4].z)
		.push((float)p[5].x).push((float)p[5].y).push((float)p[5].z)

		.push((float)p[5].x).push((float)p[5].y).push((float)p[5].z)
		.push((float)p[4].x).push((float)p[4].y).push((float)p[4].z)
		.push((float)p[6].x).push((float)p[6].y).push((float)p[6].z)

		.push((float)p[5].x).push((float)p[5].y).push((float)p[5].z)
		.push((float)p[6].x).push((float)p[6].y).push((float)p[6].z)
		.push((float)p[7].x).push((float)p[7].y).push((float)p[7].z)

		.push((float)p[5].x).push((float)p[5].y).push((float)p[5].z)
		.push((float)p[6].x).push((float)p[6].y).push((float)p[6].z)
		.push((float)p[0].x).push((float)p[0].y).push((float)p[0].z)

		.push((float)p[7].x).push((float)p[7].y).push((float)p[7].z)
		.push((float)p[0].x).push((float)p[0].y).push((float)p[0].z)
		.push((float)p[1].x).push((float)p[1].y).push((float)p[1].z)

		.push((float)p[4].x).push((float)p[4].y).push((float)p[4].z)
		.push((float)p[2].x).push((float)p[2].y).push((float)p[2].z)
		.push((float)p[0].x).push((float)p[0].y).push((float)p[0].z)

		.push((float)p[0].x).push((float)p[0].y).push((float)p[0].z)
		.push((float)p[6].x).push((float)p[6].y).push((float)p[6].z)
		.push((float)p[4].x).push((float)p[4].y).push((float)p[4].z)

		.push((float)p[1].x).push((float)p[1].y).push((float)p[1].z)
		.push((float)p[3].x).push((float)p[3].y).push((float)p[3].z)
		.push((float)p[5].x).push((float)p[5].y).push((float)p[5].z)

		.push((float)p[5].x).push((float)p[5].y).push((float)p[5].z)
		.push((float)p[1].x).push((float)p[1].y).push((float)p[1].z)
		.push((float)p[7].x).push((float)p[7].y).push((float)p[7].z);


	}

	@Override
	public void drawPolygons(Polygonizable polygons, Object obj, boolean asNode, Shader s, int highlight,
			boolean asWireframe, Matrix4d m) {
		m = getTransformation(m);
		//		PolygonMesh pm = (PolygonMesh) polygons.getPolygonizableSource(getRenderGraphState());

		float[] v = ((PolygonMesh) polygons.getPolygonizableSource(getRenderGraphState())).getVertexData().clone();

		for(int i=0; i<v.length;i+=3) {
			Point3d p = new Point3d(v[i],v[i+1],v[i+2]);
			m.transform(p);
			v[i]=(float) p.x;
			v[i+1]=(float) p.y;
			v[i+2]=(float) p.z;
		}

		vertices.addAll(v, 0, v.length);
	}

	@Override
	public void drawLine(Tuple3f start, Tuple3f end, Tuple3f color, int highlight, Matrix4d m) {



		Vector3d a = new Vector3d(end);
		double len = a.length();
		Matrix3d rot = new Matrix3d();
		Math2.getOrthogonalBasis(a,rot, true);
		m = getTransformation(m);


		Matrix4d r = new Matrix4d();
		r.setIdentity();
		r.setRotationScale(rot);

		m.mul(r);

		float x0 = -lineSize;
		float y0 = -lineSize;
		float z0 = 0;
		float x1 = lineSize;
		float y1 = lineSize;
		float z1 = (float)len;

		Point4d[] p = new Point4d[] {new Point4d (x0, y0, z0, 1),
				new Point4d (x0, y0, z1, 1), new Point4d (x1, y0, z0, 1),
				new Point4d (x1, y0, z1, 1), new Point4d (x1, y1, z0, 1),
				new Point4d (x1, y1, z1, 1), new Point4d (x0, y1, z0, 1),
				new Point4d (x0, y1, z1, 1)};

		for (int i = 0; i < p.length; i++) {
			m.transform (p[i]);
		}

		vertices.push((float)p[1].x).push((float)p[1].y).push((float)p[1].z)
		.push((float)p[0].x).push((float)p[0].y).push((float)p[0].z)
		.push((float)p[2].x).push((float)p[2].y).push((float)p[2].z)

		.push((float)p[1].x).push((float)p[1].y).push((float)p[1].z)
		.push((float)p[2].x).push((float)p[2].y).push((float)p[2].z)
		.push((float)p[3].x).push((float)p[3].y).push((float)p[3].z)

		.push((float)p[3].x).push((float)p[3].y).push((float)p[3].z)
		.push((float)p[2].x).push((float)p[2].y).push((float)p[2].z)
		.push((float)p[4].x).push((float)p[4].y).push((float)p[4].z)

		.push((float)p[3].x).push((float)p[3].y).push((float)p[3].z)
		.push((float)p[4].x).push((float)p[4].y).push((float)p[4].z)
		.push((float)p[5].x).push((float)p[5].y).push((float)p[5].z)

		.push((float)p[5].x).push((float)p[5].y).push((float)p[5].z)
		.push((float)p[4].x).push((float)p[4].y).push((float)p[4].z)
		.push((float)p[6].x).push((float)p[6].y).push((float)p[6].z)

		.push((float)p[5].x).push((float)p[5].y).push((float)p[5].z)
		.push((float)p[6].x).push((float)p[6].y).push((float)p[6].z)
		.push((float)p[7].x).push((float)p[7].y).push((float)p[7].z)

		.push((float)p[5].x).push((float)p[5].y).push((float)p[5].z)
		.push((float)p[6].x).push((float)p[6].y).push((float)p[6].z)
		.push((float)p[0].x).push((float)p[0].y).push((float)p[0].z)

		.push((float)p[7].x).push((float)p[7].y).push((float)p[7].z)
		.push((float)p[0].x).push((float)p[0].y).push((float)p[0].z)
		.push((float)p[1].x).push((float)p[1].y).push((float)p[1].z)

		.push((float)p[4].x).push((float)p[4].y).push((float)p[4].z)
		.push((float)p[2].x).push((float)p[2].y).push((float)p[2].z)
		.push((float)p[0].x).push((float)p[0].y).push((float)p[0].z)

		.push((float)p[0].x).push((float)p[0].y).push((float)p[0].z)
		.push((float)p[6].x).push((float)p[6].y).push((float)p[6].z)
		.push((float)p[4].x).push((float)p[4].y).push((float)p[4].z)

		.push((float)p[1].x).push((float)p[1].y).push((float)p[1].z)
		.push((float)p[3].x).push((float)p[3].y).push((float)p[3].z)
		.push((float)p[5].x).push((float)p[5].y).push((float)p[5].z)

		.push((float)p[5].x).push((float)p[5].y).push((float)p[5].z)
		.push((float)p[1].x).push((float)p[1].y).push((float)p[1].z)
		.push((float)p[7].x).push((float)p[7].y).push((float)p[7].z);

	}

	@Override
	public void drawPointCloud(float[] locations, float pointSize, Tuple3f color, int highlight, Matrix4d t) {
		// TODO Auto-generated method stub

	}

	@Override
	public void drawParallelogram(float axis, Vector3f secondAxis, float scaleU, float scaleV, Shader s, int highlight,
			boolean asWireframe, Matrix4d m) {
		m = getTransformation(m);
		double x0 = -secondAxis.x;
		double z0 = 0;
		double x1 = secondAxis.x;
		double z1 = axis;

		Point4d[] p = new Point4d[] {
				new Point4d (x0, 0, z0, 1), new Point4d (x0, 0, z1, 1),
				new Point4d (x1, 0, z0, 1), new Point4d (x1, 0, z1, 1)};

		for (int i = 0; i < p.length; i++) {
			m.transform (p[i]);
		}

		vertices.push((float)p[1].x).push((float)p[1].y).push((float)p[1].z)
		.push((float)p[0].x).push((float)p[0].y).push((float)p[0].z)
		.push((float)p[2].x).push((float)p[2].y).push((float)p[2].z)
		.push((float)p[1].x).push((float)p[1].y).push((float)p[1].z)
		.push((float)p[2].x).push((float)p[2].y).push((float)p[2].z)
		.push((float)p[3].x).push((float)p[3].y).push((float)p[3].z);	



		// TODO Auto-generated method stub
	}

	@Override
	public void drawPlane(Shader s, int highlight, boolean asWireframe, Matrix4d m) {
		m = getTransformation(m);
		float w = 10;
		float l = 10;

		double x0 = -w/2d;
		double y0 = -l/2d;
		double x1 = w/2d;
		double y1 = l/2d;

		Point4d[] p = new Point4d[] {
				new Point4d (x0, y0, 0, 1), new Point4d (x0, y1, 0, 1),
				new Point4d (x1, y0, 0, 1), new Point4d (x1, y1, 0, 1)};

		for (int i = 0; i < p.length; i++) {
			m.transform (p[i]);
		}

		vertices.push((float)p[1].x).push((float)p[1].y).push((float)p[1].z)
		.push((float)p[0].x).push((float)p[0].y).push((float)p[0].z)
		.push((float)p[2].x).push((float)p[2].y).push((float)p[2].z)
		.push((float)p[1].x).push((float)p[1].y).push((float)p[1].z)
		.push((float)p[2].x).push((float)p[2].y).push((float)p[2].z)
		.push((float)p[3].x).push((float)p[3].y).push((float)p[3].z);	
	}

	@Override
	public void drawSphere(float radius, Shader s, int highlight, boolean asWireframe, Matrix4d m) {
		m = getTransformation(m);
		final int uCount = 15;
		final int vCount = 15;

		Point3d[] lastRow = new Point3d[uCount+1];

		Point3d start = new Point3d(0, 0, -radius);
		m.transform(start);

		float theta = (float) (Math.PI * ((float) 1 / (float) vCount - 0.5f));
		lastRow[0] = locateOnSphere(0, theta, uCount, radius);
		m.transform(lastRow[0]);
		for (int u = 1; u < uCount+1; u++) {
			lastRow[u] = locateOnSphere(u, theta, uCount, radius);
			m.transform(lastRow[u]);
			vertices.push((float) start.x).push((float) start.y).push((float) start.z).push((float) lastRow[u].x)
			.push((float) lastRow[u].y).push((float) lastRow[u].z).push((float) lastRow[u - 1].x)
			.push((float) lastRow[u - 1].y).push((float) lastRow[u - 1].z);
		}
		vertices.push((float) start.x).push((float) start.y).push((float) start.z).push((float) lastRow[0].x)
		.push((float) lastRow[0].y).push((float) lastRow[0].z).push((float) lastRow[uCount-1].x)
		.push((float) lastRow[uCount - 1].y).push((float) lastRow[uCount - 1].z);

		for (int v = 2; v < vCount; v++) {
			theta = (float) (Math.PI * ((float) v / (float) vCount - 0.5f));

			Point3d oldPos= locateOnSphere(0, theta, uCount, radius);
			m.transform(oldPos);
			vertices.push((float) oldPos.x).push((float) oldPos.y).push((float) oldPos.z)
			.push((float) lastRow[0].x).push((float) lastRow[0].y).push((float) lastRow[0].z)
			.push((float) lastRow[1].x).push((float) lastRow[1].y).push((float) lastRow[1].z);
			Point3d rowStart = new Point3d(lastRow[0]);
			lastRow[0]=oldPos;

			for (int u = 1; u < uCount+1; u++) {
				Point3d newPos = locateOnSphere(u, theta, uCount, radius);
				m.transform(newPos);

				vertices.push((float) newPos.x).push((float) newPos.y).push((float) newPos.z)
				.push((float) lastRow[u].x).push((float) lastRow[u].y).push((float) lastRow[u].z)
				.push((float) lastRow[u - 1].x).push((float) lastRow[u - 1].y).push((float) lastRow[u - 1].z);

				if(u<uCount-1) {
					vertices.push((float) newPos.x).push((float) newPos.y).push((float) newPos.z)
					.push((float) lastRow[u].x).push((float) lastRow[u].y).push((float) lastRow[u].z)
					.push((float) lastRow[u + 1].x).push((float) lastRow[u + 1].y).push((float) lastRow[u + 1].z);
				}else {
					vertices.push((float) newPos.x).push((float) newPos.y).push((float) newPos.z)
					.push((float) lastRow[u].x).push((float) lastRow[u].y).push((float) lastRow[u].z)
					.push((float) rowStart.x).push((float) rowStart.y).push((float) rowStart.z);

				}
				lastRow[u]=newPos;
			}
		}

		Point3d end = new Point3d(0, 0, radius);
		m.transform(end);

		for (int u = 1; u < uCount+1; u++) {
			lastRow[u] = locateOnSphere(u, theta, uCount, radius);
			m.transform(lastRow[u]);
			vertices.push((float) end.x).push((float) end.y).push((float) end.z).push((float) lastRow[u].x)
			.push((float) lastRow[u].y).push((float) lastRow[u].z).push((float) lastRow[u - 1].x)
			.push((float) lastRow[u - 1].y).push((float) lastRow[u - 1].z);
		}



	}

	private Point3d locateOnSphere(float u, float theta, float uCount, float r) {
		float phi = (float) (Math.PI * 2 * u / uCount);
		float cosPhi = (float) Math.cos(phi);
		float sinPhi = (float) Math.sin(phi);
		float cosTheta = (float) Math.cos(theta);
		float sinTheta = (float) Math.sin(theta);
		float x = r * cosPhi * cosTheta;
		float y = r * sinPhi * cosTheta;
		float z = r * sinTheta;
		return new Point3d(x, y, z);

	}

	@Override
	public void drawTextBlock(String caption, java.awt.Font font, float depth, Shader s, int highlight,
			boolean asWireframe, Matrix4d t) {
		// TODO Auto-generated method stub

	}

	@Override
	public void drawSupershape(float a, float b, float m1, float n11, float n12, float n13, float m2, float n21,
			float n22, float n23, Shader s, int highlight, boolean asWireframe, Matrix4d t) {
		// TODO Auto-generated method stub

	}

	@Override
	public void drawLamella(float halfWidth, float halfLength, float height, float a, float b, Shader s, int highlight,
			boolean asWireframe, Matrix4d t) {
		// TODO Auto-generated method stub

	}

	@Override
	public void drawFrustum(float height, float baseRadius, float topRadius, boolean baseClosed, boolean topClosed,
			float scaleV, Shader s, int highlight, boolean asWireframe, Matrix4d m) {
		m = getTransformation(m);

		//		float[] p = new float[156]; // (25+25+2)*3
		Point3d start = new Point3d(0, 0, 0);
		m.transform(start);

		Point3d startTop = new Point3d(0, 0, height);
		m.transform(startTop);

		//		vertices.push ((float) start.x).push ((float) start.y).push ((float) start.z);
		float phi = (float) (Math.PI * 2 * 0 / 25);
		float cosPhi = (float) Math.cos(phi);
		float sinPhi = (float) Math.sin(phi);
		Point3d old = new Point3d(baseRadius * cosPhi, baseRadius * sinPhi, 0);
		Point3d oldTop = new Point3d(topRadius * cosPhi, topRadius * sinPhi, height);

		m.transform(old);
		m.transform(oldTop);

		for (int u = 1; u < 27; u++) {
			phi = (float) (Math.PI * 2 * u / 25);
			cosPhi = (float) Math.cos(phi);
			sinPhi = (float) Math.sin(phi);

			Point3d ne = new Point3d(baseRadius * cosPhi, baseRadius * sinPhi, 0);
			Point3d neTop = new Point3d(topRadius * cosPhi, topRadius * sinPhi, height);

			m.transform(ne);
			m.transform(neTop);

			vertices.push((float) start.x).push((float) start.y).push((float) start.z).push((float) old.x)
			.push((float) old.y).push((float) old.z).push((float) ne.x).push((float) ne.y).push((float) ne.z);

			vertices.push((float) startTop.x).push((float) startTop.y).push((float) startTop.z).push((float) oldTop.x)
			.push((float) oldTop.y).push((float) oldTop.z).push((float) neTop.x).push((float) neTop.y)
			.push((float) neTop.z);

			vertices.push((float) old.x).push((float) old.y).push((float) old.z).push((float) oldTop.x)
			.push((float) oldTop.y).push((float) oldTop.z).push((float) neTop.x).push((float) neTop.y)
			.push((float) neTop.z);

			vertices.push((float) neTop.x).push((float) neTop.y).push((float) neTop.z).push((float) old.x)
			.push((float) old.y).push((float) old.z).push((float) ne.x).push((float) ne.y).push((float) ne.z);

			oldTop = new Point3d(neTop);
			old = new Point3d(ne);

		}

	}

	@Override
	public void drawPrismRectangular(float y, float xPos, float xNeg, float zPos, float zNeg, int highlight,
			boolean asWireframe, Matrix4d t) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean getWindowPos(Tuple3f location, Tuple2f out) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void drawRectangle(int x, int y, int w, int h, Tuple3f color) {
		// TODO Auto-generated method stub

	}

	@Override
	public void fillRectangle(int x, int y, int w, int h, Tuple3f color) {
		// TODO Auto-generated method stub

	}

	@Override
	public void drawString(int x, int y, String text, java.awt.Font font, Tuple3f color) {
		// TODO Auto-generated method stub

	}

	@Override
	public void drawFrustumIrregular(float height, int sectorCount, float[] baseRadii, float[] topRadii,
			boolean baseClosed, boolean topclosed, float scaleV, Shader s, int highlight, boolean asWireframe,
			Matrix4d t) {
		// TODO Auto-generated method stub

	}

	@Override
	public void drawSphereSegmentSolid(float radius, float theta1, float theta2, float phi, Shader s, int highlight,
			boolean asWireframe, Matrix4d t) {
		// TODO Auto-generated method stub

	}

	public boolean shouldContinue() {
		return vertices.shouldContinue;
	}

}
