package de.grogra.mesh.renderer;

import javax.vecmath.Matrix4d;

import de.grogra.graph.GraphState;
import de.grogra.graph.Path;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.DisplayVisitor;
import de.grogra.imp3d.Renderable;
import de.grogra.imp3d.View3D;
import de.grogra.imp3d.shading.Shader;
import de.grogra.mesh.utils.FloatListArray;
import de.grogra.pf.registry.Item;
import de.grogra.pf.ui.Workbench;
import de.grogra.util.Utils;

public class MergeShapeVisitor extends DisplayVisitor {
	VertexCollector collector;
	Node root = null;
	float boxSize;
	float lineSize;
	boolean minimalDisplay;
	
	public MergeShapeVisitor() {
		this(0f,0f);
	}

	public MergeShapeVisitor(float boxSize, float lineSize) {
		
		Item opts = Item.resolveItem(Workbench.current(), "/ui/view3dOptions/meshRender");
		this.boxSize=boxSize;
		this.lineSize=lineSize;
		this.minimalDisplay = Utils.getBoolean(opts, "minimalDisplay",true);
		if(boxSize==0) {
			this.boxSize=Utils.getFloat(opts, "boxSize", 0.2f);
		}
		if(lineSize==0) {
			this.lineSize=Utils.getFloat(opts, "lineSize", 0.1f);
		}
	}
	public MergeShapeVisitor(float boxSize, float lineSize,boolean minimalDisplay) {
		this(boxSize,lineSize);
		this.minimalDisplay=minimalDisplay;
	}
	
	public void init (GraphState gs, View3D view, boolean checkLayer)
	{
		Matrix4d m = new Matrix4d();
		m.setIdentity();
		init (gs, gs.getGraph().getTreePattern(),
			m, view, checkLayer);
		
		this.collector= new VertexCollector(this);
	}
	
	public void init (GraphState gs, View3D view, boolean checkLayer, Node root)
	{
		init(gs, view, checkLayer);
		this.root=root;
	}
	
	@Override
	protected Object visitEnter (Object object, boolean asNode, Path path)
	{
		Object o = super.visitEnter(object, asNode, path);
		if (!collector.shouldContinue()) { return STOP; }
		return o;
	}
	
	@Override
	protected void visitImpl (Object object, boolean asNode, Shader s,
			Path path)
	{
		if (object == root) {
			//The collector goes through the root twice. Once from the main visitor, once 
			// trough itself. So its local transfo is applied twice. We start at O,O,O for 
			// the collection. Thus, set identity
			transformation.setIdentity(); 
			return;
		}
		
		Object shape = state.getObjectDefault (object, asNode,
			de.grogra.imp3d.objects.Attributes.SHAPE, null);
		if (shape != null)
		{
			if (shape instanceof Renderable)
			{
				((Renderable) shape).draw (object, asNode,
						collector);
			}
			//TODO: support NURBS & supershapes
		}
	}
	
	public FloatListArray getVertices() {
		return collector.getVertices();
	}
}
