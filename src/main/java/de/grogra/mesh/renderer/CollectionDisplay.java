package de.grogra.mesh.renderer;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import de.grogra.imp.PickList;
import de.grogra.imp.ResolutionSensitive;
import de.grogra.imp3d.Pickable;
import de.grogra.imp3d.RenderState;
import de.grogra.imp3d.Renderable;
import de.grogra.imp3d.objects.Attributes;
import de.grogra.imp3d.objects.ColoredNull;

/**
 * Intermediate node in a collection cloud that "hide" the display of the point in the graph bellow.
 * In the case of pointcloud as graph, this node should have one parent (the collection cloud) and one child:
 * a pointcloudBase. 
 * It visit the graph bellow and gather the display information from these nodes. As long as the collection 
 * is not updated, the nodes bellow are never re visited for display.
 */
public abstract class CollectionDisplay extends ColoredNull implements Renderable, Pickable, ResolutionSensitive {

	protected boolean update;
	//enh:field getter setter
	
	protected float boxSize;
	//enh:field getter setter

	protected float lineSize;
	//enh:field getter setter

	State state = State.NORMAL;

	public CollectionDisplay()
	{
		this.update=true;
		this.setResolution(3);
	}
	
	@Override
	public State getState() {
		return state;
	}
	
	@Override
	public void setState(State s) {
		this.state=s;
	}
	
	@Override
	public void draw(Object object, boolean asNode, RenderState rs) {
		switch(state) {
			case NORMAL: 
				break;
			case STOPPED:
				drawImpl(object, asNode, rs);
				setState(State.NORMAL);// should probably be set at the visitor level
				break;
			case PASSED:
				break;
		}
	}
	
	protected abstract void drawImpl(Object object, boolean asNode, RenderState rs);

	@Override
	public void pick(Object object, boolean asNode, Point3d origin, Vector3d direction, Matrix4d transformation,
			PickList list) {
		switch(state) {
			case NORMAL: 
				break;
			case STOPPED:
				pickImpl(object, asNode, origin, direction, transformation, list);
				setState(State.NORMAL);// should probably be set at the visitor level
				break;
			case PASSED:
				break;
		}
	}
	
	protected abstract void pickImpl(Object object, boolean asNode, Point3d origin, Vector3d direction, Matrix4d transformation,
			PickList list);
	

	
	// enh:insert $TYPE.addIdentityAccessor (Attributes.SHAPE);
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;

	public static final NType.Field update$FIELD;
	public static final NType.Field boxSize$FIELD;
	public static final NType.Field lineSize$FIELD;

	private static final class _Field extends NType.Field
	{
		private final int id;

		_Field (String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			super (CollectionDisplay.$TYPE, name, modifiers, type, componentType);
			this.id = id;
		}

		@Override
		public void setBoolean (Object o, boolean value)
		{
			switch (id)
			{
				case 0:
					((CollectionDisplay) o).update = (boolean) value;
					return;
			}
			super.setBoolean (o, value);
		}

		@Override
		public boolean getBoolean (Object o)
		{
			switch (id)
			{
				case 0:
					return ((CollectionDisplay) o).isUpdate ();
			}
			return super.getBoolean (o);
		}

		@Override
		public void setFloat (Object o, float value)
		{
			switch (id)
			{
				case 1:
					((CollectionDisplay) o).boxSize = (float) value;
					return;
				case 2:
					((CollectionDisplay) o).lineSize = (float) value;
					return;
			}
			super.setFloat (o, value);
		}

		@Override
		public float getFloat (Object o)
		{
			switch (id)
			{
				case 1:
					return ((CollectionDisplay) o).getBoxSize ();
				case 2:
					return ((CollectionDisplay) o).getLineSize ();
			}
			return super.getFloat (o);
		}
	}

	static
	{
		$TYPE = new NType (CollectionDisplay.class);
		$TYPE.addManagedField (update$FIELD = new _Field ("update", _Field.PROTECTED  | _Field.SCO, de.grogra.reflect.Type.BOOLEAN, null, 0));
		$TYPE.addManagedField (boxSize$FIELD = new _Field ("boxSize", _Field.PROTECTED  | _Field.SCO, de.grogra.reflect.Type.FLOAT, null, 1));
		$TYPE.addManagedField (lineSize$FIELD = new _Field ("lineSize", _Field.PROTECTED  | _Field.SCO, de.grogra.reflect.Type.FLOAT, null, 2));
		$TYPE.addIdentityAccessor (Attributes.SHAPE);
		$TYPE.validate ();
	}

	public boolean isUpdate ()
	{
		return update;
	}

	public void setUpdate (boolean value)
	{
		this.update = (boolean) value;
	}

	public float getBoxSize ()
	{
		return boxSize;
	}

	public void setBoxSize (float value)
	{
		this.boxSize = (float) value;
	}

	public float getLineSize ()
	{
		return lineSize;
	}

	public void setLineSize (float value)
	{
		this.lineSize = (float) value;
	}

//enh:end

}
