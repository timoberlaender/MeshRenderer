package de.grogra.mesh.renderer.handler;

import de.grogra.imp.ResolutionSensitive;
import de.grogra.imp3d.Pickable;
import de.grogra.imp3d.Renderable;

/**
 * Nodes whose display in the graph are handled by collectiondisplay object should implements this.
 */
public interface CollectionDisplayable extends Renderable, Pickable, ResolutionSensitive, Updatable {

	public CollectionDisplayHandler getDisplayHandler();
	public void setDisplayHandler(CollectionDisplayHandler d);
}
