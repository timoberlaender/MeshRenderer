package de.grogra.mesh.renderer.handler;

import static java.lang.Math.max;
import static java.lang.Math.min;

import javax.vecmath.Color3f;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import de.grogra.graph.ArrayPath;
import de.grogra.graph.impl.GraphManager;
import de.grogra.graph.impl.Node;
import de.grogra.imp.PickList;
import de.grogra.imp.View;
import de.grogra.imp3d.RenderState;
import de.grogra.imp3d.View3D;
import de.grogra.imp3d.objects.ColoredNull;
import de.grogra.math.RGBColor;
import de.grogra.mesh.renderer.MergeShapeVisitor;
import de.grogra.mesh.utils.FloatListArray;
import de.grogra.pf.ui.Workbench;

/**
 * Similar to CollectionPoint but designed to not be part of the graph
 */
public class CollectionPointHandler extends CollectionDisplayHandler {

	// store point locations (x,y,z) as triple of floats
	transient float[] points;

	private transient double minx, miny, minz, maxx, maxy, maxz;

	
	public CollectionPointHandler() {
		super();
		points = new float[0];
	}
	
	@Override
	public void dispose() {
		points = null;
	}
	
	@Override
	public CollectionDisplayHandler createNew(Node parent) {
		return new CollectionPointHandler().init(parent);
	}

	@Override
	protected void drawImpl(Object object, boolean asNode, RenderState rs) {
		loadPoints(rs, View3D.getDefaultView (Workbench.current()));
		if (points!=null) {
			rs.drawPointCloud(points, getPointSize(), getColor(), RenderState.CURRENT_HIGHLIGHT, null);
		}
	}

	private float getPointSize() {
		if (getNode() instanceof de.grogra.imp3d.objects.PointCloudImpl) {
			return ((de.grogra.imp3d.objects.PointCloudImpl)getNode()).getPointSize();
		}
		return 3.0f;
	}
	private Color3f getColor() {
		if (getNode() instanceof ColoredNull) {
			return ((ColoredNull)getNode()).getColor();
		}
		return RGBColor.BLACK;
	}


	
	@Override
	protected void pickImpl(Object object, boolean asNode, Point3d origin, Vector3d direction, Matrix4d transformation,
			PickList list) {
		// ray-box intersection from:
		// http://www.cs.utah.edu/~awilliam/box/
		// (Smit's algorithm)

		double tmin, tmax, tymin, tymax, tzmin, tzmax;
		if (direction.x >= 0) {
			tmin = (minx - origin.x) / direction.x;
			tmax = (maxx - origin.x) / direction.x;
		} else {
			tmin = (maxx - origin.x) / direction.x;
			tmax = (minx - origin.x) / direction.x;
		}
		if (direction.y >= 0) {
			tymin = (miny - origin.y) / direction.y;
			tymax = (maxy - origin.y) / direction.y;
		} else {
			tymin = (maxy - origin.y) / direction.y;
			tymax = (miny - origin.y) / direction.y;
		}
		if (tmin > tymax || tymin > tmax)
			return;
		if (tymin > tmin)
			tmin = tymin;
		if (tymax < tmax)
			tmax = tymax;
		if (direction.z >= 0) {
			tzmin = (minz - origin.z) / direction.z;
			tzmax = (maxz - origin.z) / direction.z;
		} else {
			tzmin = (maxz - origin.z) / direction.z;
			tzmax = (minz - origin.z) / direction.z;
		}
		if (tmin > tzmax || tzmin > tmax)
			return;
		if (tzmin > tmin)
			tmin = tzmin;
		if (tzmax < tmax)
			tmax = tzmax;

		list.add(tmin);
	}

	//This load the polygons in the shape array. If the array is empty return an empty mesh
	private void loadPoints(RenderState rs, View v) {
		if (!update) {
			return;
		}
		points=null;
		this.setUpdate(false);
		FloatListArray vertices = getVertices(rs,v);
		points = vertices.toArray(3);

		final int N = this.points.length / 3;
		minx = miny = minz = Double.POSITIVE_INFINITY;
		maxx = maxy = maxz = Double.NEGATIVE_INFINITY;
		for (int i = 0; i < N; i++)
		{
			minx = min(minx, points[3*i+0]);
			miny = min(miny, points[3*i+1]);
			minz = min(minz, points[3*i+2]);
			maxx = max(maxx, points[3*i+0]);
			maxy = max(maxy, points[3*i+1]);
			maxz = max(maxz, points[3*i+2]);
		}		
	}

	public FloatListArray getVertices(RenderState rs, View v) {
		MergeShapeVisitor visitor = new MergeShapeVisitor();
		visitor.init(rs.getRenderGraphState(), (View3D) v, false, getNode());
		if (getNode().getGraph() != null) {
			getNode().getGraph().accept(getNode(), visitor, null);
		} else {
			ArrayPath p = new ArrayPath(v.getGraph());
			GraphManager.acceptGraph(getNode(), visitor, p);
		}
		return visitor.getVertices();
	}

	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;


	static
	{
		$TYPE = new NType (new CollectionPointHandler ());
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new CollectionPointHandler ();
	}

//enh:end
}
