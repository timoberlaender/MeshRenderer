package de.grogra.mesh.renderer.handler;

import javax.vecmath.Color3f;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import de.grogra.graph.impl.Node;
import de.grogra.imp.PickList;
import de.grogra.imp3d.RenderState;
import de.grogra.imp3d.objects.BoundedCloud;
import de.grogra.imp3d.objects.Cloud;
import de.grogra.imp3d.objects.ColoredNull;
import de.grogra.imp3d.objects.PointCloud;
import de.grogra.imp3d.objects.PointCloudImpl;
import de.grogra.math.RGBColor;

public class FloatArrayHandler extends CollectionDisplayHandler {

	
	public FloatArrayHandler() {
		super();
	}

	@Override
	public void dispose() {
	}
	
	@Override
	public CollectionDisplayHandler createNew(Node parent) {
		return new FloatArrayHandler().init(parent);
	}

	@Override
	protected void drawImpl(Object object, boolean asNode, RenderState rs) {
		if (getNode() instanceof PointCloud) {
			rs.drawPointCloud(((PointCloud)getNode()).getCloud().pointsToFloat(), getPointSize(), 
					getColor(), RenderState.CURRENT_HIGHLIGHT, null);
		}
	}
	
	private float getPointSize() {
		if (getNode() instanceof PointCloudImpl) {
			return ((de.grogra.imp3d.objects.PointCloudImpl)getNode()).getPointSize();
		}
		return 3.0f;
	}
	private Color3f getColor() {
		if (getNode() instanceof ColoredNull) {
			return ((ColoredNull)getNode()).getColor();
		}
		return RGBColor.BLACK;
	}

	@Override
	protected void pickImpl(Object object, boolean asNode, Point3d origin, Vector3d direction, Matrix4d transformation,
			PickList list) {
		if (getNode() instanceof PointCloud) {
			//			// ray-box intersection from:
			//			// http://www.cs.utah.edu/~awilliam/box/
			//			// (Smit's algorithm)
			Cloud cloud = ((PointCloud)getNode()).getCloud();
			if (cloud != null);
			if (!(cloud instanceof BoundedCloud)) { return; }

			double minx = (double) ((BoundedCloud)cloud).getMinimumX();
			double miny = (double) ((BoundedCloud)cloud).getMinimumY();
			double minz = (double) ((BoundedCloud)cloud).getMinimumZ();
			double maxx = (double) ((BoundedCloud)cloud).getMaximumX();
			double maxy = (double) ((BoundedCloud)cloud).getMaximumY();
			double maxz = (double) ((BoundedCloud)cloud).getMaximumZ();

			double tmin, tmax, tymin, tymax, tzmin, tzmax;
			if (direction.x >= 0) {
				tmin = (minx - origin.x) / direction.x;
				tmax = (maxx - origin.x) / direction.x;
			} else {
				tmin = (maxx - origin.x) / direction.x;
				tmax = (minx - origin.x) / direction.x;
			}
			if (direction.y >= 0) {
				tymin = (miny - origin.y) / direction.y;
				tymax = (maxy - origin.y) / direction.y;
			} else {
				tymin = (maxy - origin.y) / direction.y;
				tymax = (miny - origin.y) / direction.y;
			}
			if (tmin > tymax || tymin > tmax)
				return;
			if (tymin > tmin)
				tmin = tymin;
			if (tymax < tmax)
				tmax = tymax;
			if (direction.z >= 0) {
				tzmin = (minz - origin.z) / direction.z;
				tzmax = (maxz - origin.z) / direction.z;
			} else {
				tzmin = (maxz - origin.z) / direction.z;
				tzmax = (minz - origin.z) / direction.z;
			}
			if (tmin > tzmax || tzmin > tmax)
				return;
			if (tzmin > tmin)
				tmin = tzmin;
			if (tzmax < tmax)
				tmax = tzmax;

			list.add(tmin);

		}
	}

	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;


	static
	{
		$TYPE = new NType (new FloatArrayHandler ());
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new FloatArrayHandler ();
	}

//enh:end
}
