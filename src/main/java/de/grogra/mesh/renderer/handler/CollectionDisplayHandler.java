package de.grogra.mesh.renderer.handler;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import de.grogra.graph.impl.Node;
import de.grogra.imp.PickList;
import de.grogra.imp.ResolutionSensitive;
import de.grogra.imp3d.Pickable;
import de.grogra.imp3d.RenderState;
import de.grogra.imp3d.Renderable;
import de.grogra.pf.registry.Item;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.util.ButtonWidget;

/**
 * A collectiondisplay object. The node is the parent
 */
public abstract class CollectionDisplayHandler extends Node implements Renderable, Pickable, ResolutionSensitive {
		
	transient protected boolean update=true;
	transient State state = State.NORMAL;
	transient Node parent;
	
	public CollectionDisplayHandler()
	{
		this.update=true;
	}
	
	public CollectionDisplayHandler init(Node p) {
		setNode(p);
		return this;
	}
	
	protected Node getNode() {
		return parent;
	}
	
	public void setNode(Node n) {
		parent = n;
	}
	@Override
	public State getState() {
		return state;
	}
		
	public static void update(Item item, Object info, Context ctx)
	{
		if (info instanceof ButtonWidget)
		{
			info = ((ButtonWidget) info).getProperty().getValue();
			if (info instanceof CollectionDisplayHandler)
			{
				CollectionDisplayHandler viewer = (CollectionDisplayHandler) info;
				if (viewer.getNode() instanceof Updatable) {
					((Updatable)viewer.getNode()).update();
				} else {
					viewer.setUpdate(true);
				}
			}
		}
	}
	
	@Override
	public void setState(State s) {
		this.state=s;
	}
	
	public boolean getUpdate() {
		return update;
	}
	
	public void setUpdate(boolean b) {
		this.update=b;
	}
	
	public abstract void dispose();
	
	public abstract CollectionDisplayHandler createNew(Node parent);
	
	@Override
	public void draw(Object object, boolean asNode, RenderState rs) {
		switch(state) {
			case NORMAL: 
				break;
			case STOPPED:
				drawImpl(object, asNode, rs);
				((CollectionDisplayable)parent).setState(State.NORMAL);// should probably be set at the visitor level
				break;
			case PASSED:
				break;
		}
	}
	
	protected abstract void drawImpl(Object object, boolean asNode, RenderState rs);

	@Override
	public void pick(Object object, boolean asNode, Point3d origin, Vector3d direction, Matrix4d transformation,
			PickList list) {
		switch(state) {
			case NORMAL: 
				break;
			case STOPPED:
				pickImpl(object, asNode, origin, direction, transformation, list);
				((CollectionDisplayable)parent).setState(State.NORMAL);// should probably be set at the visitor level
				break;
			case PASSED:
				break;
		}
	}
	
	protected abstract void pickImpl(Object object, boolean asNode, Point3d origin, Vector3d direction, Matrix4d transformation,
			PickList list);
	

	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;


	static
	{
		$TYPE = new NType (CollectionDisplayHandler.class);
		$TYPE.validate ();
	}

//enh:end
	
}
