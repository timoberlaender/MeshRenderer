package de.grogra.mesh.renderer.handler;

public interface Updatable {

	public void setUpdate(boolean b);
	public boolean getUpdate();
	public void update();
}
