package de.grogra.mesh.utils;

import de.grogra.imp3d.objects.PolygonMesh;

public class PolygonMeshArray extends PolygonMesh {

	public void setIndexData( int[] indexData ) {
		this.indices = indexData;
		incrementStamp( );
	}

	public void setVertexData( float[] vertexData ) {

		this.v = vertexData;
		incrementStamp( );
	}
}
