package de.grogra.mesh.utils;

import java.util.ArrayList;
import java.util.List;

import de.grogra.xl.util.FloatList;

public class FloatListArray extends FloatList {

	//999972 factor of 9 for meshes and 12 for points
//	int sizeOfElements = 999972;
	int sizeOfElements = 12144;
//	int currentSize = 0;
	public FloatList currentList;
	List<float[]> allLists;

	/**
	 * True if there is enough memory to add more to the list
	 */
	public boolean shouldContinue = true;

	public FloatListArray ()
	{
		allLists = new ArrayList<float[]>();
		currentList = safelyCreate(sizeOfElements);
	}

	public FloatListArray (int size)
	{
		sizeOfElements = size;
		allLists = new ArrayList<float[]>();
		currentList = safelyCreate(sizeOfElements);
	}

	public FloatList safelyCreate(int size) {
		try {
			return new FloatList(sizeOfElements);
		} catch (OutOfMemoryError e) {
			shouldContinue = false;
			System.err.println("Loading some of the pointcloud into the "
					+ "collector failed due to insufficient memory");
		}
		return new FloatList();
	}

	public float[] safelyCast(FloatList l) {
		try {
			return l.toArray();
		} catch (OutOfMemoryError e) {
			shouldContinue = false;
			System.err.println("Loading some of the pointcloud into the "
					+ "collector failed due to insufficient memory");
		}
		return new float[0];
	}


	/**
	 * If the capacity is not high enough, the currentList is added to the 
	 * list of lists and a new FloatList is created
	 */
	@Override
	public void ensureCapacity (int capacity)
	{
		if (capacity > currentList.elements.length)
		{
			allLists.add(safelyCast(currentList));
			currentList = safelyCreate(sizeOfElements);
		}
	}
	
	private void createNext(int capacity) {
		allLists.add(safelyCast(currentList));
		currentList = safelyCreate(capacity);
	}

	@Override
	public boolean add (float o)
	{
		if (currentList.size == sizeOfElements)
		{
			ensureCapacity (currentList.size + 1);
			if (!shouldContinue) { return false; }
		}
		currentList.elements[currentList.size] = o;
		currentList.size++;
		size++;
		return true;
	}

	@Override
	public FloatList  addAll (float[] v, int begin, int length)
	{
		if (!shouldContinue) { return this; }
		if (length <=  sizeOfElements - currentList.size) {
			System.arraycopy(v, begin, currentList.elements, currentList.size, length);
			size+=length;
			currentList.size+=length;
		} else {
			int remains = length - (sizeOfElements - currentList.size);
			int begin_remains = begin + (sizeOfElements - currentList.size);
			System.arraycopy(v, begin, currentList.elements, currentList.size, sizeOfElements - currentList.size);
			size += length - remains;
			currentList.size += length - remains;
			createNext(sizeOfElements);
			if (!shouldContinue) { return this; }
			addAll(v, begin_remains, remains);
		}
		return this;
	}

	@Override
	public float[] toArray ()
	{
		return toArray(1);
	}
	
	/**
	 * This toArray is specifically made for the collection display, where each object HAS to be
	 * a set of x floats.
	 */
	public float[] toArray (int x)
	{
		createNext(0);
		int r = size % x;
		return reduce(r);
	}

	/**
	 * Returns an array of the size - i of the elements
	 */
	public float[] reduce (int i)
	{
		if (size - i< 0) { return new float[0]; }
		float[] a = new float[size-i];
		arraycopyAll (this, 0, a, 0, size-i);
		return a;
	}

	@Override
	public float[] toArray (float[] array)
	{
//		int totalSize = allLists.size() * sizeOfElements + currentList.elements.length;
		
		int l;
		if ((l = array.length) > size)
		{
			array[size] = 0;
		}
		else if (l < size)
		{
			array = new float[size];
		}
		arraycopyAll (this, 0, array, 0, size);
		return array;
	}

	public static void arraycopyAll (FloatListArray src, int srcIndex,
			float[] dest, int destIndex, int length)
	{
		int i = 0;
		int remains = length;
		for (float[] l : src.allLists) {
			System.arraycopy (l, 0, dest, i*src.sizeOfElements, (l.length < remains)? l.length : remains );
			remains -= l.length;
			l=null;
			i++;
		}
	}
	
	
	@Override
	public Object clone ()
	{
		throw new RuntimeException("Maybe to implements one day.");
	}

	@Override
	public void trimToSize ()
	{
		throw new RuntimeException("Maybe to implements one day.");
	}

	@Override
	public void add (int index, float o)
	{
		throw new RuntimeException("Maybe to implements one day.");
	}
	
	@Override
	public void addIfNotContained (float o)
	{
		throw new RuntimeException("Maybe to implements one day.");
	}

	@Override
	public FloatList  addAll (FloatList v)
	{
		throw new RuntimeException("Maybe to implements one day.");
	}

	@Override
	public float removeAt (int index)
	{
		throw new RuntimeException("Maybe to implements one day.");
	}

	@Override
	public boolean remove (float o)
	{
		throw new RuntimeException("Maybe to implements one day.");
	}

	@Override
	public float set (int index, float o)
	{
		throw new RuntimeException("Maybe to implements one day.");
	}

	@Override
	public float get (int index)
	{
		throw new RuntimeException("Maybe to implements one day.");
	}

	@Override
	public float peek (int index)
	{
		throw new RuntimeException("Maybe to implements one day.");
	}

	@Override
	public boolean contains (float o)
	{
		throw new RuntimeException("Maybe to implements one day.");
	}
	
	@Override
	public int indexOf (float o)
	{
		throw new RuntimeException("Maybe to implements one day.");
	}

	@Override
	public int lastIndexOf (float o)
	{
		throw new RuntimeException("Maybe to implements one day.");
	}

	@Override
	public int binarySearch (float value)
	{
		throw new RuntimeException("Maybe to implements one day.");
	}
	
	@Override
	public void writeTo (java.nio.FloatBuffer out)
	{
		throw new RuntimeException("Maybe to implements one day.");
	}
	
	@Override
	public float pop ()
	{
		throw new RuntimeException("Maybe to implements one day.");
	}
	
	@Override
	public void setSize (int size)
	{
		throw new RuntimeException("Maybe to implements one day.");
	}

	@Override
	public boolean equals (Object o)
	{
		throw new RuntimeException("Maybe to implements one day.");
	}

	@Override
	public void consume (float value)
	{
		throw new RuntimeException("Maybe to implements one day.");
	}
	
	@Override
	public void evaluateFloat (de.grogra.xl.lang.FloatConsumer cons)
	{
		throw new RuntimeException("Maybe to implements one day.");
	}

	@Override
	public void values (de.grogra.xl.lang.FloatConsumer cons)
	{
		throw new RuntimeException("Maybe to implements one day.");
	}
}
